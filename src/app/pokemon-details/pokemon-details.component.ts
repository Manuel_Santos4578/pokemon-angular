import { Component, OnInit, signal, WritableSignal } from '@angular/core';
import { PokemonApiService } from '../pokemon-api.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Pokemon } from '@src/models/pokemon';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.scss'],
  imports: [RouterLink, TitleCasePipe]
})
export class PokemonDetailsComponent implements OnInit {

  public pokemonName: string;
  public sprite: string;
  public nextPokemon: string = "";
  public previousPokemon: string = "";
  public pokemon: WritableSignal<Pokemon>;
  isShiny: boolean;
  isClosed: boolean;
  position: "front" | "back" = "front";

  constructor(
    private route: ActivatedRoute,
    private pokemonsService: PokemonApiService) {
    this.isShiny = false;
    this.isClosed = false;
    this.sprite = "";
    this.pokemonName = "";
    this.pokemon = signal(<Pokemon>{})
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => this.pokemonName = params.get('pokemonName') as string);
    this.getPokemonByName(this.pokemonName);
  }

  ngOnDestroy() {
  }

  async getPokemonByName(name: string) {
    this.pokemon.set(await this.pokemonsService.getPokemonByName(name));
    this.sprite = this.getPic();
    await this.getPreviousPokemonById(this.pokemon().id - 1);
    await this.getNextPokemonById(this.pokemon().id + 1);
  }

  getPic(sprite: "back_default" | "back_female" | "front_default" | "front_female" = 'front_default'): string {
    const sprites = {
      back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/",
      back_female: "",
      front_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",
      front_female: "",
    }
    let pokeSprite: string = sprites[sprite] as string;
    pokeSprite += this.isShiny ? 'shiny/' : '';
    pokeSprite += this.pokemon().id + '.png';
    return pokeSprite;
  }

  async getPreviousPokemonById(pokemonId: number) {
    this.previousPokemon = (await this.pokemonsService.getPokemonById(pokemonId)).name
  }

  async getNextPokemonById(pokemonId: number) {
    const next_pokemon = await this.pokemonsService.getPokemonById(pokemonId)
    this.nextPokemon = next_pokemon.name;
  }

  toggleShiny() {
    this.isShiny = this.isShiny ? false : true;
    this.sprite = this.getPic(`${this.position}_default`);
  }

  togglePosition() {
    this.position = this.position === 'front' ? 'back' : 'front';
    this.sprite = this.getPic(`${this.position}_default`)
  }

  toggleOpenClosed() {
    this.isClosed = !this.isClosed;
  }
  async navigateToPokemon(name: string): Promise<void> {
    this.pokemonName = name;
    await this.getPokemonByName(this.pokemonName);
  }
}
