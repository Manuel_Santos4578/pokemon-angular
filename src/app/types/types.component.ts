import { Component, OnInit, signal, WritableSignal } from '@angular/core';
import { ApiDTO } from '@src/models/ApiDTO';

import { PokemonApiService } from '../pokemon-api.service';
import { Type } from '@src/models/type';
import { RouterLink } from '@angular/router';
import { CacheService } from '../cache.service';

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss'],
  imports: [RouterLink]
})
export class TypesComponent implements OnInit {

  public apiTypes: WritableSignal<ApiDTO<Type>> = signal(<ApiDTO<Type>>{});
  public pageArray: WritableSignal<number[]> = signal([]);
  public pageSize = 24;
  public currentPage = 1;
  constructor(private cache: CacheService, private service: PokemonApiService) {
  }

  ngOnInit(): void {
    this.getTypes();
  }

  createPageArray(count: number) {
    this.pageArray.set([]);
    for (let i = 1; i <= count; i++) {
      this.pageArray().push(i);
    }
  }

  async getTypes() {
    const response = await this.cache.getOrAddAsync(`type_lis`, async() => this.service.getTypes())
    this.apiTypes().results = response.results;
    // this.apiTypes().count = response.count;
    const pageCount = Math.ceil(response.count / this.pageSize);
    this.createPageArray(pageCount);
  }

}
