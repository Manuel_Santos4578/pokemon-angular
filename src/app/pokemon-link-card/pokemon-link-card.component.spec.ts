import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonLinkCardComponent } from './pokemon-link-card.component';

describe('PokemonLinkCardComponent', () => {
  let component: PokemonLinkCardComponent;
  let fixture: ComponentFixture<PokemonLinkCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PokemonLinkCardComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonLinkCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
