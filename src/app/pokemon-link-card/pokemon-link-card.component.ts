import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Pokemon } from '@src/models/pokemon';

@Component({
  selector: 'app-pokemon-link-card',
  imports: [RouterLink],
  templateUrl: './pokemon-link-card.component.html',
  styleUrl: './pokemon-link-card.component.scss'
})
export class PokemonLinkCardComponent implements OnInit {
  @Input("pokemon") public pokemon!: Pokemon;

  ngOnInit(): void {

  }
}
