import { Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { PokemonsComponent } from './pokemons/pokemons.component';
import { PokemonDetailsComponent } from './pokemon-details/pokemon-details.component';
import { TypesComponent } from './types/types.component';
import { TypeDetailsComponent } from './type-details/type-details.component';

export const routes: Routes = [
    {
        path: '',
        title: 'Landing Page',
        component: LandingComponent
    },
    {
        path: 'pokemons',
        title: 'Pokemon List Page',
        component: PokemonsComponent
    },
    {
        path: 'pokemons/:pokemonName',
        title: 'Pokemon Details page',
        component: PokemonDetailsComponent
    },
    {
        path: 'types',
        title: 'Types List',
        component: TypesComponent
    },
    {
        path: 'types/:typeName',
        title: 'Types Detail',
        component: TypeDetailsComponent
    },
    {
        path: '**',
        title: 'Fallback Page',
        component: LandingComponent
    }
];
