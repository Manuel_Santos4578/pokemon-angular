import { Component, OnInit, signal, WritableSignal } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Type } from '@src/models/type';
import { PokemonApiService } from '../pokemon-api.service';
import { PokemonLinkCardComponent } from '../pokemon-link-card/pokemon-link-card.component';

@Component({
  selector: 'app-type-details',
  templateUrl: './type-details.component.html',
  styleUrls: ['./type-details.component.scss'],
  imports: [PokemonLinkCardComponent]
})
export class TypeDetailsComponent implements OnInit {

  constructor(private service: PokemonApiService, private route: ActivatedRoute) { }

  public type: WritableSignal<Type> = signal(<Type>{});
  private typeName: string = "";

  ngOnInit(): void {
    this.route.paramMap.subscribe(async params => {
      this.typeName = params.get('typeName') as string;
      await this.getType(this.typeName);
    });
  }

  async getType(name: string) {
    this.type.set(await this.service.getTypeByName(name))
  }
}
