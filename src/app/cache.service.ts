import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private cache = new Map<string, { expire: number, val: any }>();

  constructor() { }

  async getOrAddAsync(key: string, callback: Function) {
    if (!this.cache.has(key)) {
      await this.set(key, callback);
    }
    const cache_val = this.cache.get(key)!;
    if (cache_val?.expire < new Date().getTime()) {
      await this.set(key, callback);
    }
    return this.cache.get(key)!.val;
  }

  has(key: string) {
    return this.cache.has(key);
  }

  get(key: string) {
    this.cache.get(key)?.val;
  }

  async set(key: string, callback: Function) {
    const expiration = new Date().getTime() + (30 * 60 * 1000) // 30 min
    this.cache.set(key, { expire: expiration, val: await callback() });
  }

  remove(key: string) {
    this.cache.delete(key);
  }

  clear() {
    this.cache.clear();
  }
}
