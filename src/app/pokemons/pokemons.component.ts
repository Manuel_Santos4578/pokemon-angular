import { Component, OnInit, signal, WritableSignal } from '@angular/core';
import { PokemonApiService } from '@src/app/pokemon-api.service';
import { ApiDTO } from '@src/models/ApiDTO';
import { ActivatedRoute, RouterLink, RouterModule } from '@angular/router';
import { Pokemon } from '@src/models/pokemon';
import { CacheService } from '../cache.service';
import { PokemonLinkCardComponent } from '../pokemon-link-card/pokemon-link-card.component';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss'],
  imports: [RouterLink, RouterModule, PokemonLinkCardComponent]
})
export class PokemonsComponent implements OnInit {

  public apiPokemons: WritableSignal<ApiDTO<Pokemon>> = signal<ApiDTO<Pokemon>>(<ApiDTO<Pokemon>>{});
  public pageArray: WritableSignal<number[]> = signal([]);
  public pageSize = 24;
  public currentPage = 1;
  constructor(private cache: CacheService, private service: PokemonApiService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(async params => {
      this.currentPage = !isNaN(parseInt(params["p"])) ? parseInt(params["p"]) : this.currentPage;
      this.pageSize = !isNaN(parseInt(params["s"])) ? parseInt(params["s"]) : this.pageSize;
      await this.getPokemons((this.currentPage - 1) * this.pageSize, this.pageSize);
    })
  }

  ngOnDestroy() {

  }

  createPageArray(count: number) {
    this.pageArray.set([]);
    for (let i = 1; i <= count; i++) {
      this.pageArray().push(i);
    }
  }

  async getPokemons(skip: number = 0, take: number = 3000) {
    const api_result = await this.cache.getOrAddAsync(`pokemon_list_${skip}_${take}`, async () => await this.service.getPokemons(skip, take)) as ApiDTO<Pokemon>
    this.apiPokemons().results = api_result.results;
    // this.apiPokemons().count = api_result.count;
    const pageCount = Math.ceil(api_result.count / this.pageSize);
    this.createPageArray(pageCount);
  }

  async changePage() {
    this.route.queryParams.subscribe(async params => {
      this.currentPage = !isNaN(parseInt(params["p"])) ? parseInt(params["p"]) : this.currentPage;
      this.pageSize = !isNaN(parseInt(params["s"])) ? parseInt(params["s"]) : this.pageSize;
      await this.getPokemons((this.currentPage - 1) * this.pageSize, this.pageSize);
    })
  }
}
