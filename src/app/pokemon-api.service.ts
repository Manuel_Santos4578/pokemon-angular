import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon';
import { Type } from '../models/type';
import { ApiDTO } from '../models/ApiDTO';
import { ApiGeneration } from '@src/models/generations';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {

  private readonly apiDomain = 'https://pokeapi.co/api/v2/';

  constructor() {
  }

  async getPokemons(skip: number = 0, take: number = 3000): Promise<ApiDTO<Pokemon>> {
    return (await (await fetch(`${this.apiDomain}pokemon?limit=${take}&offset=${skip}`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as ApiDTO<Pokemon>
  }

  async getGenerations(): Promise<ApiGeneration> {
    return (await (await fetch(`${this.apiDomain}generation`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as ApiGeneration
  }

  async getPokemonByName(name: string): Promise<Pokemon> {
    return (await (await fetch(`${this.apiDomain}pokemon/${name}`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as Pokemon
  }

  async getPokemonById(id: number): Promise<Pokemon> {
    return (await (await fetch(`${this.apiDomain}pokemon/${id}`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as Pokemon
  }

  async getTypes(): Promise<ApiDTO<Type>> {
    return (await (await fetch(`${this.apiDomain}type`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as ApiDTO<Type>
  }

  async getTypeByName(name: string): Promise<Type> {
    return (await (await fetch(`${this.apiDomain}type/${name}`, {
      headers: {
        "Accept": "application/json"
      },
    })).json()) as Type
  }
}
