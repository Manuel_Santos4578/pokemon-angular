import { DamageRelations } from "./damage-relations";
import { Pokemon } from "./pokemon";

export interface Type {
    name: string;
    url: string;
    damage_relations: DamageRelations;
    pokemon: TypePokemon[];
}

interface TypePokemon{
    pokemon: Pokemon
    slot: number
}