export interface ApiDTO<T> {
    count: number;
    results: T[];
}
