import { Type } from './type';

export interface Pokemon {
  height: number;
  id: number;
  name: string;
  types: PokemonType[];
  sprite: string;
  weight: number;
  url: string;

}

interface PokemonType {
  slot: number;
  type: Type
}