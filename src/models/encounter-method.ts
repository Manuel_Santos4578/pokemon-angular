export interface EncounterMethod {
  id: number;
  name: string;
  order: number;
}