import { DamageFrom } from "./damage-from";

export interface DamageRelations {
    double_damage_from: DamageFrom[];
    double_damage_to: DamageFrom[];
    half_damage_from: DamageFrom[];
    half_damage_to: DamageFrom[];
    no_damage_from: DamageFrom[];
    no_damage_to: DamageFrom[];
}
