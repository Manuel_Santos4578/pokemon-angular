# base image
FROM node:22-alpine as build

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install

# add app
COPY . /app

# generate build
RUN ng build --output-path=dist

# base image
FROM nginx:1.16.0-alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf

# copy artifact build from the 'build environment'
COPY --from=build /app/dist/browser/ /usr/share/nginx/html

# expose port 80
EXPOSE 80

RUN chmod 755 -R /usr/share/nginx/html/assets/*

# run nginx
CMD ["nginx", "-g", "daemon off;"]