#!/usr/bin/env bash

(cd ../../ && docker build -t pokemon-angular .)

(cd ../../ && docker run -d --name pokemon-angular -p 4200:80 pokemon-angular)
